import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import axios from "axios";

export default class CreateUser extends Component {
  constructor(props) {
    super(props);

    // binding this to these methods because we wrote this out of our constructor
    this.onChangeUserName = this.onChangeUserName.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      username: ""
    };
  }

  onChangeUserName(e) {
    this.setState({
      username: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    const user = {
      username: this.state.username
    };

    console.log(user);

    axios
      .post("http://localhost:5000/users/add", user)
      .then(res => console.log(res.data));

    this.setState({
      username: ""
    });
  }

  render() {
    return (
      <Form onSubmit={this.onSubmit}>
        <h4> Create New User</h4>
        <br />
        <Form.Group controlId="formBasicUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            required
            value={this.state.username}
            onChange={this.onChangeUserName}
          ></Form.Control>
        </Form.Group>

        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    );
  }
}
